package ga.manuelgarciacr.tripmemories.model;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RestApiService {
    @GET("tripsServlet?op=list")
    Call<ArrayList<Trip>> allTrips ();
    @GET("tripsServlet?op=get")
    Call<Trip> getTripById (@Query("tripid") String tripid);
    @POST("tripsServlet?op=insert")
    Call<Trip> insertTrip (@Body Trip trip);
    @GET("tripsServlet?op=delete")
    Call<Void> deleteTrip (@Query("tripid") String tripid);
}