package ga.manuelgarciacr.tripmemories.model;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

@SuppressWarnings("unused")
@Database(entities = {Trip.class}, version = 1, exportSchema = false)
@TypeConverters(TripTypeConverter.class)
abstract class TripDatabase extends RoomDatabase {
    private static volatile TripDatabase INSTANCE;

    abstract TripDao tripDao();

    static TripDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (TripDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            TripDatabase.class, "trip_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
        private final TripDao mDao;

        PopulateDbAsync(TripDatabase db) {
            mDao = db.tripDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            mDao.deleteAll();
            Trip trip = new Trip("Vacances estiu a Venècia", "Itàlia");
            mDao.insert(trip);
            trip = new Trip("Vacances estiu a Lisboa", "Portugal");
            mDao.insert(trip);
            trip = new Trip("Cap de setmana a Bilbao", "Espanya");
            mDao.insert(trip);
            trip = new Trip("Vacances Setmana Santa a Roma", "Itàlia");
            mDao.insert(trip);
            trip = new Trip("Cap de setmana a Madrid", "Espanya");
            mDao.insert(trip);
            return null;
        }
    }
}