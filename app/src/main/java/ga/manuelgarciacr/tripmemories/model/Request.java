package ga.manuelgarciacr.tripmemories.model;

import androidx.annotation.NonNull;

import java.util.UUID;

public class Request<T>{
    private UUID uuid;
    private int st;
    private T t;
    private String aux;

    private Request (UUID uuid, int st, T t, String aux){
        super();
        if(uuid == null)
            uuid = UUID.randomUUID();
        setUuid(uuid);
        setSt(st);
        setT(t);
        setAux(aux);
    }

    public Request(T t, String aux){
        this(null, 0, t, aux);
    }

    @SuppressWarnings("unused")
    private Request (T t){
        this(null, 0, t, "");
    }

    public Request(String aux){
        this(null, 0, null, aux);
    }

    @SuppressWarnings("unused")
    private Request(){
        this(null, 0,null, "");
    }

    public Request getRequestResponse(int st, T t){
        return new Request<>(getUuid(), st, t, getAux());
    }

    @SuppressWarnings("unused")
    public Request getRequestResponse(Integer st){
        return new Request<>(getUuid(), st, null, getAux());
    }

    public UUID getUuid() {
        return uuid;
    }

    private void setUuid(UUID uuid){
        this.uuid = uuid;
    }

    public Integer getSt() {
        return st;
    }

    private void setSt(Integer st) {
        this.st = st;
    }

    public T getT() {
        return t;
    }

    private void setT(T t) {
        this.t = t;
    }

    public String getAux() {
        return aux;
    }

    private void setAux(String aux) {
        this.aux = aux;
    }

    @NonNull
    @Override
    public String toString() {
        return getUuid().toString() + " : " + getSt() + " : " + getAux();
    }
}
