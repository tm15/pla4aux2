package ga.manuelgarciacr.tripmemories.model;

import android.view.View;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;

import com.google.android.material.snackbar.Snackbar;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import ga.manuelgarciacr.tripmemories.util.Pgb;

public class RequestObserver {
    private Pgb pgb;
    private View view;
    private HashMap<UUID, Date> requestMap;

    public RequestObserver(Pgb pgb, View view) {
        setPgb(pgb);
        setView(view);
        requestMap = new HashMap<>();
    }

    public <T> void addObserver(LiveData<Request> liveData, LifecycleOwner lifeCycleOwner, String prompt, String errorMsg, UUID uuid, Action<T> action){
        liveData.observe(lifeCycleOwner, (request) -> {
            if (uuid != null && uuid != request.getUuid())
                return;
            if (request.getSt() == 0) {
                if (requestMap.containsKey(request.getUuid()))
                    return;
                requestMap.put(request.getUuid(), new Date());
                getPgb().add(prompt);
                return;
            }
            Date d = requestMap.remove(request.getUuid());
            if (d == null)
                return;
            getPgb().remove();
            if (request.getSt() < 0) {
                Snackbar.make(getView(), errorMsg, Snackbar.LENGTH_LONG)
                        .show();
                return;
            }
            if(action != null)
                //noinspection unchecked
                action.action(request);
        });
    }

    public interface Action <T> {
        void action(Request<T> request);
    }

    private Pgb getPgb() {
        return pgb;
    }

    private void setPgb(Pgb pgb) {
        this.pgb = pgb;
    }

    private View getView(){
        return view;
    }

    private void setView(View view) {
        this.view = view;
    }
}
