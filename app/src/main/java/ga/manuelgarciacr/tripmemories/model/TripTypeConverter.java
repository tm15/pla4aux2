package ga.manuelgarciacr.tripmemories.model;

import androidx.room.TypeConverter;

import java.util.Date;
import java.util.UUID;

@SuppressWarnings("WeakerAccess")
class TripTypeConverter {

    @TypeConverter
    public UUID toUUID(String uuid) {
        return UUID.fromString(uuid);
    }

    @TypeConverter
    public String fromUUID(UUID uuid) {
        return uuid.toString();
    }

    @TypeConverter
    public Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }
    @TypeConverter
    public Long dateToTimestamp(Date date) {
        if (date == null) {
            return null;
        } else {
            return date.getTime();
        }
    }
}
