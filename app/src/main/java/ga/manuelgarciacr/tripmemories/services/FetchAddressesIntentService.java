package ga.manuelgarciacr.tripmemories.services;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ga.manuelgarciacr.tripmemories.R;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class FetchAddressesIntentService extends IntentService {
    public FetchAddressesIntentService() {
        super("FetchAddressesIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        if(intent == null)
            return;
        ResultReceiver rec = intent.getParcelableExtra(Constants.RECEIVER);
        if(rec == null)
            return;
        String address = intent.getStringExtra(Constants.ADDRESS_EXTRA);
        String errorMessage = "";
        List<Address> addresses = null;
        Bundle b = new Bundle();

        try {
            addresses = geocoder.getFromLocationName(address, 3);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = getString(R.string.service_not_available);
            Log.e(TAG, errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid address value
            errorMessage = getString(R.string.invalid_address_used) + ". " + address;
            Log.e(TAG, errorMessage, illegalArgumentException);
        }

        if (addresses == null || addresses.size()  == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e(TAG, errorMessage);
            }
            b.putString(Constants.RESULT_DATA_KEY, errorMessage);
            rec.send(Constants.FAILURE_RESULT, b);
        } else {
            b.putParcelableArrayList(Constants.RESULT_DATA_KEY, (ArrayList<? extends Parcelable>) addresses);
            b.putString(Constants.RESULT_ORIGIN_KEY, address);
            rec.send(Constants.SUCCESS_RESULT, b);
        }
    }

    public interface Constants{
        int SUCCESS_RESULT = 0;
        int FAILURE_RESULT = 1;
        String PACKAGE_NAME = "ga.manuelgarciacr.tripmemories";
        String RECEIVER = PACKAGE_NAME + ".RECEIVER";
        String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
        String RESULT_ORIGIN_KEY = PACKAGE_NAME + ".RESULT_ORIGIN_KEY";
        String ADDRESS_EXTRA = PACKAGE_NAME + ".ADDRESS_EXTRA";
        String COUNT_EXTRA = PACKAGE_NAME + ".COUNT_EXTRA";
    }
}
