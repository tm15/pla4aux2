package ga.manuelgarciacr.tripmemories.ui.detail;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

import ga.manuelgarciacr.tripmemories.R;
import ga.manuelgarciacr.tripmemories.model.Trip;
import ga.manuelgarciacr.tripmemories.ui.home.HomeViewModel;
import ga.manuelgarciacr.tripmemories.util.DatePickerFragment;
import ga.manuelgarciacr.tripmemories.util.PictureUtils;

public class DetailFragment extends Fragment implements DatePickerFragment.Callbacks{
    public static final String ARG_TRIP_ID = "trip_id";
    private static final String ARG_DIALOG_FRAGMENT = "date_picker_fragment";
    private static final int REQUEST_DATE = 0;
    private static final int REQUEST_CONTACT = 1;
    private static final int REQUEST_PHOTO = 2;
    private HomeViewModel mViewModel;
    private View mView;
    private UUID mUuid;
    private Trip mTrip = null;
    private EditText mEdtTripCountry, mEdtTripName;
    private Button mBtnTripDate;
    private Button mTripContactButton;
    private ImageView mTripPhoto;
    private ImageButton mCameraButton, mPhoneCallButton;
    private boolean mIsNew = false;
    private File mPhotoFile;
    private Uri mPhotoUri;
    private Boolean mPhoneCallPermission;
    private boolean mEmailSent = true;

    private Animator currentAnimator;
    private int shortAnimationDuration;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null)
            mUuid = (UUID) getArguments().getSerializable(ARG_TRIP_ID);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.detail_fragment, container, false);

        mEdtTripCountry = mView.findViewById(R.id.edtTripCountry);
        mEdtTripName = mView.findViewById(R.id.edtTripName);
        mBtnTripDate = mView.findViewById(R.id.btnTripDate);
        mBtnTripDate.setOnClickListener(view -> {
            DatePickerFragment datePickerFragment = new DatePickerFragment().newInstace(mTrip.getDate());
            datePickerFragment.setTargetFragment(this, REQUEST_DATE);
            assert getFragmentManager() != null;
            datePickerFragment.show(getFragmentManager(), ARG_DIALOG_FRAGMENT);
        });
        mTripContactButton = mView.findViewById(R.id.trip_contact_button);
        mTripContactButton.setOnClickListener(view -> {
            Intent contactIntent = new Intent();
            contactIntent.setAction(Intent.ACTION_PICK);
            contactIntent.setData(Contacts.CONTENT_URI);
            startActivityForResult(contactIntent, REQUEST_CONTACT);
        });
        mPhoneCallPermission = ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED;
        mCameraButton = mView.findViewById(R.id.btnCamera);
        mTripPhoto = mView.findViewById(R.id.trip_photo);
        mTripPhoto.setOnClickListener(view -> {
            if(mTripPhoto.getDrawable() != null && mPhotoFile.exists())
                zoomImageFromThumb(mTripPhoto);
        });

        // Retrieve and cache the system's default "short" animation time.
        shortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
        mViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(HomeViewModel.class);
        mViewModel.tripLiveData.observe(getViewLifecycleOwner(), trip -> {
            mTrip = trip;
            if(mTrip == null) {
                return;
            }
            if (mTrip.getCountry().equals("") && mTrip.getName().equals(""))
                mIsNew = true;

            mPhotoFile = mViewModel.getPhoto(trip);
            mPhotoUri = FileProvider.getUriForFile(requireActivity(),
                    "ga.manuelgarciacr.tripmemories.fileprovider",
                    mPhotoFile);
            final Intent captureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            PackageManager packageManager = getActivity().getPackageManager();
            boolean canTakePhoto = mPhotoFile != null &&
                    captureImage.resolveActivity(packageManager) != null;
            updatePhotoView();
            mCameraButton.setEnabled(canTakePhoto);
            mCameraButton.setOnClickListener(view -> {
                captureImage.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
                List<ResolveInfo> cameraActivities = getActivity()
                        .getPackageManager().queryIntentActivities(captureImage,
                                PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo activity : cameraActivities) {
                    getActivity().grantUriPermission(activity.activityInfo.packageName,
                            mPhotoUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                startActivityForResult(captureImage, REQUEST_PHOTO);
            });

            updateUI();
        });
        mViewModel.loadUUID(mUuid);

        //AppCompatActivity activity = (AppCompatActivity) Objects.requireNonNull(getActivity());
        //ActionBar actionBar = Objects.requireNonNull(activity.getSupportActionBar());
        //actionBar.setSubtitle(mUuid.toString());
        //actionBar.setDisplayHomeAsUpEnabled(false);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPhoneCallButton = mView.findViewById(R.id.btnPhoneCall);
        mPhoneCallButton.setOnClickListener(mView ->{
            if(mTrip.getPhone() == null || mTrip.getPhone().equals("")){
                new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Clear travel companion")
                    .setMessage("Are you sure you want to clear the travel companion?")
                    .setPositiveButton("Yes", (dialog, which) -> {
                        mTrip.setPhone("");
                        mTrip.setCompanion("");
                        updateUI();
                    })
                    .setNegativeButton("No", null)
                    .show();
            }
            if(mTrip.getPhone() != null && !mTrip.getPhone().equals("")){
                AlertDialog.Builder dlg = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
                dlg.setTitle("Call / Clear");
                final int[] selItem = {0};
                String[] action = {"Call", "Clear travel companion"};
                dlg.setPositiveButton("Yes", (dialog, which) -> {
                    if(selItem[0] == 0 && !mPhoneCallPermission) {
                        Toast.makeText(getContext(), "Assegura't de donar permisos a l'aplicació per " +
                                "trucar per telèfon", Toast.LENGTH_LONG).show();
                    }else if(selItem[0] == 0){
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + mTrip.getPhone()));
                        startActivity(callIntent);
                    }else{
                        mTrip.setPhone("");
                        mTrip.setCompanion("");
                        updateUI();
                    }
                });
                dlg.setSingleChoiceItems(action, selItem[0], (dialog, item) -> selItem[0] = item);
                AlertDialog alert = dlg.create();
                alert.show();
            }
        });
        Button mTripSendButton = mView.findViewById(R.id.trip_send_button);
        mTripSendButton.setOnClickListener(mView -> {
            Intent intent = new Intent();
            if(!mEmailSent && mTrip.getEmail() != null && !mTrip.getEmail().equals("")) {
                intent.setAction(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:" + mTrip.getEmail()));
                intent.putExtra(Intent.EXTRA_SUBJECT, getTextToSend("subject"));
                intent.putExtra(Intent.EXTRA_TEXT, getTextToSend("question"));
                mEmailSent = true;
            }else{
                intent.setAction(Intent.ACTION_SEND);
                intent.setData(Uri.parse("mailto:" + "manuelgarciacr@gmail.com"));
                intent.putExtra(Intent.EXTRA_TEXT, getTextToSend("assertion"));
                intent.putExtra(Intent.EXTRA_SUBJECT, getTextToSend("subject"));
                intent.setType("text/plain");
            }
            Intent s = Intent.createChooser(intent, "");
            startActivity(s);
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDateSelected(Date date) {
        mTrip.setDate(date);
        updateDate();
    }

    private void updateUI() {
        mEdtTripCountry.setText(mTrip.getCountry());
        mEdtTripName.setText(mTrip.getName());
        if(mTrip.getCompanion() != null && !mTrip.getCompanion().equals("")){
            mTripContactButton.setText(mTrip.getCompanion());
            mPhoneCallButton.setVisibility(View.VISIBLE);
            if(mTrip.getPhone() != null && !mTrip.getPhone().equals("")){
                mPhoneCallButton.setImageResource(R.drawable.ic_phone_clear_36);
            }else{
                mPhoneCallButton.setImageResource(R.drawable.ic_clear_36);
            }
        }else{
            mTripContactButton.setText(R.string.company_del_viatge);
            mPhoneCallButton.setVisibility(View.GONE);
        }
        updateDate();
    }

    private void updateDate(){
        mBtnTripDate.setText(String.format(new Locale("es", "ES"), "%s %te/%2$tm/%2$tY", getString(R.string.data_del_viatge), mTrip.getDate()));
    }

    private String getTextToSend(String type) {
        switch(type){
            case "subject":
                return getString(R.string.viatge_text_per_subject, mTrip.getName(),
                        mTrip.getCountry());
            case "question":
                return getString(R.string.viatge_text_per_question);
            case "assertion":
                return getString(R.string.viatge_text_per_assertion, mTrip.getName(),
                    mTrip.getCountry(), mTrip.getCompanion());
        default:
            return "";
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CONTACT) {
            if(data == null)
                return;
            Uri contactUri = data.getData();
            String[] queryContactFields = new String[]{
                    Contacts.DISPLAY_NAME,
                    Contacts.HAS_PHONE_NUMBER,
                    Contacts._ID
            };
            ContentResolver cr = Objects.requireNonNull(getActivity()).getContentResolver();
            try(Cursor c = cr.query(Objects.requireNonNull(contactUri), queryContactFields, null, null, null)) {
                if (Objects.requireNonNull(c).getCount() == 0) {
                    return;
                }
                c.moveToFirst();
                String company = c.getString(0);
                mTrip.setCompanion(company);
                mTrip.setPhone("");
                mTrip.setEmail("");
                if(ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                        Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(getContext(), "Assegura't de donar permisos a l'aplicació per " +
                            "accedir als teus contactes", Toast.LENGTH_LONG).show();
                }else{
                    String[] queryPhoneFields = new String[]{
                            Phone.TYPE,
                            Phone.NUMBER,
                            Phone.NORMALIZED_NUMBER};
                    try (Cursor p = cr.query(
                            Phone.CONTENT_URI,
                            queryPhoneFields,
                            Phone.CONTACT_ID + " = " + c.getString(2), null, null)) {
                        int type = 0, newType;
                        String phoneNumber = null;
                        while (Objects.requireNonNull(p).moveToNext()) {
                            if (p.getString(2) != null) {
                                switch (p.getInt(0)) {
                                    case Phone.TYPE_HOME:
                                        newType = 4;
                                        break;
                                    case Phone.TYPE_MOBILE:
                                        newType = 3;
                                        break;
                                    case Phone.TYPE_WORK:
                                        newType = 2;
                                        break;
                                    default:
                                        newType = 1;
                                }
                                if (newType >= type) {
                                    type = newType;
                                    phoneNumber = p.getString(2);
                                }
                            }
                        }
                        if (type > 0)
                            mTrip.setPhone(phoneNumber);
                        else
                            mTrip.setPhone("");
                    }
                    queryPhoneFields = new String[]{
                            Email.TYPE,
                            Email.ADDRESS};
                    try (Cursor e = cr.query(
                            Email.CONTENT_URI,
                            queryPhoneFields,
                            Email.CONTACT_ID + " = " + c.getString(2), null, null)) {
                        int type = 0, newType;
                        String email = null;
                        while (Objects.requireNonNull(e).moveToNext()) {
                            if (e.getString(1) != null) {
                                switch (e.getInt(0)) {
                                    case Email.TYPE_HOME:
                                        newType = 5;
                                        break;
                                    case Email.TYPE_MOBILE:
                                        newType = 4;
                                        break;
                                    case Email.TYPE_CUSTOM:
                                        newType = 3;
                                        break;
                                    case Email.TYPE_WORK:
                                        newType = 2;
                                        break;
                                    default:
                                        newType = 1;
                                }
                                if (newType >= type) {
                                    type = newType;
                                    email = e.getString(1);
                                }
                            }
                        }
                        if (type > 0)
                            mTrip.setEmail(email);
                        else
                            mTrip.setEmail("");
                    }
                }
            }
            mEmailSent = false;
            updateUI();
        } else if (requestCode == REQUEST_PHOTO) {
            Objects.requireNonNull(getActivity()).revokeUriPermission(mPhotoUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            updatePhotoView();
        }
    }

    private void updatePhotoView() {
        if (mPhotoFile == null || !mPhotoFile.exists()) {
            mTripPhoto.setImageDrawable(null);
        } else {
            Bitmap bitmap = PictureUtils.getScaledBitmap(
                    mPhotoFile.getPath(), Objects.requireNonNull(getActivity()));
            mTripPhoto.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Boolean countryIsEmpty = TextUtils.isEmpty(mEdtTripCountry.getText());
        Boolean nameIsEmpty = TextUtils.isEmpty(mEdtTripName.getText());
        if (countryIsEmpty || nameIsEmpty) {
            if (mIsNew) {
                mViewModel.deleteTrip(mTrip);
                Snackbar.make(mView, "Trip not saved. Name or country is empty", Snackbar.LENGTH_LONG)
                        .show();
            }else{
                Snackbar.make(mView, "Trip not updated. Name or country is empty", Snackbar.LENGTH_LONG)
                        .show();
            }
            return;
        }
        mTrip.setCountry(mEdtTripCountry.getText().toString());
        mTrip.setName(mEdtTripName.getText().toString());
        mViewModel.updateTrip(mTrip);
    }

    private void zoomImageFromThumb(final View thumbView) {
        if (currentAnimator != null) {
            currentAnimator.cancel();
        }
        final ImageView expandedImageView = mView.findViewById(
                R.id.imvExpandedImage);
        expandedImageView.setZ(12);
        Bitmap bitmap = PictureUtils.getScaledBitmap(
                mPhotoFile.getPath(), Objects.requireNonNull(getActivity()));
        expandedImageView.setImageBitmap(bitmap);
        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        mView.findViewById(R.id.container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(shortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                View bg = mView.findViewById(R.id.imvBackgroung);
                View btn = mView.findViewById(R.id.btnClearPhoto);
                bg.setZ(10);
                btn.setZ(14);
                bg.setVisibility(View.VISIBLE);
                btn.setVisibility(View.VISIBLE);
                btn.setOnClickListener(v -> {
                    mTripPhoto.setImageDrawable(null);
                            //noinspection ResultOfMethodCallIgnored
                            mPhotoFile.delete();
                    mTrip.setPhoto("");
                    expandedImageView.callOnClick();
                }
                );
                currentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                currentAnimator = null;
            }
        });
        set.start();
        currentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(view -> {
            if (currentAnimator != null) {
                currentAnimator.cancel();
            }

            // Animate the four positioning/sizing properties in parallel,
            // back to their original values.
            AnimatorSet set1 = new AnimatorSet();
            set1.play(ObjectAnimator
                    .ofFloat(expandedImageView, View.X, startBounds.left))
                    .with(ObjectAnimator
                            .ofFloat(expandedImageView,
                                    View.Y,startBounds.top))
                    .with(ObjectAnimator
                            .ofFloat(expandedImageView,
                                    View.SCALE_X, startScaleFinal))
                    .with(ObjectAnimator
                            .ofFloat(expandedImageView,
                                    View.SCALE_Y, startScaleFinal));
            set1.setDuration(shortAnimationDuration);
            set1.setInterpolator(new DecelerateInterpolator());
            set1.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    View bg = mView.findViewById(R.id.imvBackgroung);
                    View btn = mView.findViewById(R.id.btnClearPhoto);
                    bg.setVisibility(View.GONE);
                    btn.setVisibility(View.GONE);
                    thumbView.setAlpha(1f);
                    expandedImageView.setVisibility(View.GONE);
                    currentAnimator = null;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    thumbView.setAlpha(1f);
                    expandedImageView.setVisibility(View.GONE);
                    currentAnimator = null;
                }
            });
            set1.start();
            currentAnimator = set1;
        });
    }
}
