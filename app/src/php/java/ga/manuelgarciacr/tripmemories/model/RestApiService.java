package ga.manuelgarciacr.tripmemories.model;

import java.util.ArrayList;

import ga.manuelgarciacr.tripmemories.BuildConfig;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RestApiService {
    @GET("cifotestp4.php")
    Call<ArrayList<Trip>> allTrips();
    @GET("cifotestp4.php")
    Call<ArrayList<Trip>> getTripById(@Query("tripid") String tripid);
    @POST("cifotestpostp4.php")
    Call<Trip> insertTrip(@Body Trip trip);
    @HTTP(method = "DELETE", path = "cifotestpostp4.php", hasBody = true)
    Call<Void> deleteTrip(@Body Trip trip);

    Call<Void> deleteTrip(String uuid); // No se usa
}